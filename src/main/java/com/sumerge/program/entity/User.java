package com.sumerge.program.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "USERS")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "User.findAllUsers", query = "SELECT u.userPublicInfo FROM User u WHERE u.role = \"user\" AND  u.deleted = false"),
        @NamedQuery(name = "User.findAllAdmins", query = "SELECT u.userPublicInfo FROM User u WHERE u.role = \"admin\" AND  u.deleted = false"),
        @NamedQuery(name = "User.findDeletedUsers", query = "SELECT u.userPublicInfo FROM User u WHERE u.role = \"user\" AND  u.deleted = true"),
        @NamedQuery(name = "User.findDeletedAdmins", query = "SELECT u.userPublicInfo FROM User u WHERE u.role = \"admin\" AND  u.deleted = true"),
        @NamedQuery(name = "User.findUserById", query = "SELECT u.userPublicInfo FROM User u WHERE u.role = \"user\" AND  u.deleted = false AND u.id = :id"),
        @NamedQuery(name = "User.findUserByUsername", query = "SELECT u.userPublicInfo FROM User u WHERE u.role = \"user\" AND  u.deleted = false AND u.userPublicInfo.username = :username"),
        @NamedQuery(name = "User.adminFindUserById", query = "SELECT u FROM User u WHERE u.id = :id"),
        @NamedQuery(name = "User.adminFindUserByUsername", query = "SELECT u FROM User u WHERE u.userPublicInfo.username = :username"),
        @NamedQuery(name = "User.getGroupsById", query = "SELECT u.groups FROM User u WHERE u.deleted = false AND u.id = :id"),
        @NamedQuery(name = "User.getGroupsByUsername", query = "SELECT u.groups FROM User u WHERE u.deleted = false AND u.userPublicInfo.username = :username")
})
public class User implements Serializable {

    private static final long serialVersionUID = 5L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true, updatable = false)
    private long id;

    @JsonIgnore
    @Column(name = "PASSWORD", nullable = false, length = 256)
    private String password;

    @Column(name = "ROLE", nullable = false)
    private String role;

    @Column(name = "DELETED")
    private boolean deleted;

    @Embedded
    private UserPublicInfo userPublicInfo;


    @JsonIgnore
    @ManyToMany (fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "USER_GROUPS",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
    List<Group> groups;

    public User() {

    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", deleted=" + deleted +
                ", name='" + userPublicInfo.getName() + '\'' +
                ", email='" + userPublicInfo.getEmail() + '\'' +
                ", username='" + userPublicInfo.getUsername() + '\'' +
                ", groups=" + groups +
                '}';
    }

    public User(UserRegisteration userRegisteration) {
        this.password = userRegisteration.getPassword();
        this.userPublicInfo = new UserPublicInfo(userRegisteration.getUsername(), userRegisteration.getName(), userRegisteration.getEmail());
        String role = userRegisteration.getRole();
        if(role != null && role.equals("admin"))
            this.role = role;
        else
            this.role = "user";
        this.groups = new ArrayList<Group>();
    }

    public long getId() {
        return id;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public UserPublicInfo getUserPublicInfo() {
        return userPublicInfo;
    }

    public void setUserPublicInfo(UserPublicInfo userPublicInfo) {
        this.userPublicInfo = userPublicInfo;
    }


}
