package com.sumerge.program.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "USERS")
@Embeddable

public class UserPublicInfo implements Serializable {
    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "EMAIL", length = 256)
    private String email;

    @Column(name = "USERNAME", length = 128, nullable = false, unique = true)
    private String username;

    public UserPublicInfo() {

    }

    public UserPublicInfo(String username, String name, String email) {
        this.name = name;
        this.email = email;
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
