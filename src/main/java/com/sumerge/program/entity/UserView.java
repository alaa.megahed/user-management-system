package com.sumerge.program.entity;

public class UserView {

    private long id;

    private UserPublicInfo userPublicInfo;

    private boolean deleted;


    public UserView(long id, UserPublicInfo userPublicInfo, boolean deleted) {
        this.id = id;
        this.userPublicInfo = userPublicInfo;
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserPublicInfo getUserPublicInfo() {
        return userPublicInfo;
    }

    public void setUserPublicInfo(UserPublicInfo userPublicInfo) {
        this.userPublicInfo = userPublicInfo;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
