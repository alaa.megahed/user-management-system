package com.sumerge.program.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "GROUPS")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Group.findAll", query = "SELECT g.groupInfo from Group g"),
        @NamedQuery(name = "Group.findGroupById", query = "SELECT g from Group g WHERE g.groupInfo.id = :id"),
        @NamedQuery(name = "Group.findGroupMembers", query = "SELECT g.members from Group g WHERE g.groupInfo.id = :id"),

})
public class Group implements Serializable {
    @Embedded
    private GroupInfo groupInfo;

    @JsonIgnore
    @ManyToMany (fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "USER_GROUPS",
            joinColumns = @JoinColumn(name = "GROUP_ID"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID"))
    List<User> members;

    public Group() {}

    public Group(String name) {
        this.groupInfo = new GroupInfo(name);
        this.members = new ArrayList<User>();
    }


    public GroupInfo getGroupInfo() {
        return groupInfo;
    }

    public void setGroupInfo(GroupInfo groupInfo) {
        this.groupInfo = groupInfo;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object obj) {
        return this.groupInfo.getId() == ((Group) obj).getGroupInfo().getId();
    }
}
