package com.sumerge.program.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;


@Entity
@Table(name = "AUDIT_LOG")
@XmlRootElement
public class AuditLog {

    @Id
    @Column(name = "ACTION_TIME")
    private LocalDateTime actionTime;

    @Column(name = "ACTION_NAME", nullable = false)
    private String actionName;

    @Column(name = "ACTION_AUTHOR", nullable = false)
    private String actionAuthor;

    @Column(name = "ENTITY_JSON")
    private String entityJson;

    public AuditLog() {

    }

    public AuditLog(String actionName, String actionAuthor, String entityJson) {
        this.actionTime =  LocalDateTime.now();
        this.actionName = actionName;
        this.actionAuthor = actionAuthor;
        this.entityJson = entityJson;
    }

    public LocalDateTime getActionTime() {
        return actionTime;
    }

    public void setActionTime(LocalDateTime actionTime) {
        this.actionTime = actionTime;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionAuthor() {
        return actionAuthor;
    }

    public void setActionAuthor(String actionAuthor) {
        this.actionAuthor = actionAuthor;
    }

    public String getEntityJson() {
        return entityJson;
    }

    public void setEntityJson(String entityJson) {
        this.entityJson = entityJson;
    }
}
