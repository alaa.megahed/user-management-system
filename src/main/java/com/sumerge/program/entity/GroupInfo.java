package com.sumerge.program.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "GROUPS")
@Embeddable
public class GroupInfo implements Serializable {

    @Id
    @Column(name = "ID", nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NAME", nullable = false, unique = true, length = 256)
    private  String name;

    public GroupInfo() {

    }

    public GroupInfo(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
