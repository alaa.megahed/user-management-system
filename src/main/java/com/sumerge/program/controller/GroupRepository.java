package com.sumerge.program.controller;

import com.sumerge.program.entity.*;
import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;
import org.apache.log4j.Logger;
import com.google.gson.Gson;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class GroupRepository {

    private static final Logger LOGGER = Logger.getLogger(GroupRepository.class);

    private Gson gson = new Gson();

    @PersistenceContext
    private EntityManager em;

    public List<GroupInfo> getAllGroups(String actionAuthor) {
        LOGGER.debug("Entering getAllGroups method.");
        try{
            TypedQuery query = em.createNamedQuery("Group.findAll", Group.class);
            List<GroupInfo> groups = query.getResultList();
            AuditLog logRecord = new AuditLog("VIEW_ALL_GROUPS", actionAuthor, gson.toJson(groups));
            em.persist(logRecord);
            return groups;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean isMember(Group group, String username){
        if(group != null){
            List<User> members = group.getMembers();
            for(User u : members){
                if(u.getUserPublicInfo().getUsername().equals(username))
                    return true;
            }
        }
        return false;
    }

    public Group getGroupById(String actionAuthor, boolean isAdmin, long id) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering getGroupById method.");
        try{
            Group group = em.createNamedQuery("Group.findGroupById", Group.class).setParameter("id", id).getSingleResult();
            if(!isAdmin && !isMember(group, actionAuthor)){
                throw new UnauthorizedException("You are not allowed to access this data");
            }
            AuditLog logRecord = new AuditLog("VIEW_GROUP_BY_ID", actionAuthor, gson.toJson(group.getGroupInfo()));
            em.persist(logRecord);
            return group;
        } catch (NoResultException e){
            throw new ResourceNotFoundException("Group Not Found");
        }
    }

    public List<UserPublicInfo> getGroupMembers(String actionAuthor, boolean isAdmin, long id) throws UnauthorizedException {
        LOGGER.debug("Entering getGroupMembers method.");
        try{
            TypedQuery query = em.createNamedQuery("Group.findGroupMembers", Group.class).setParameter("id", id);
            List<User> members = query.getResultList();
            boolean isMember = false;
            List<UserPublicInfo> membersInfo = new ArrayList<UserPublicInfo>();
            for (User u : members){
                if(u == null)
                    break;
                if(u.getUserPublicInfo().getUsername().equals(actionAuthor))
                    isMember = true;
                if(!u.isDeleted())
                    membersInfo.add(u.getUserPublicInfo());
            }
            if(!isAdmin && !isMember){
                throw new UnauthorizedException("You are not allowed to access this data");
            }
            AuditLog logRecord = new AuditLog("VIEW_GROUP_BY_ID", actionAuthor, gson.toJson(membersInfo));
            em.persist(logRecord);
            return membersInfo;
        } catch (Exception e){
            throw e;
        }
    }

    public Group addGroup(String actionAuthor, String name) throws DatabaseException {
        LOGGER.debug("Entering addGroup method.");
        try{
            Group group = new Group(name);
            em.persist(group);
            AuditLog logRecord = new AuditLog("ADD_GROUP", actionAuthor, gson.toJson(group));
            em.persist(logRecord);
            return group;
        } catch (Exception e) {
            throw new DatabaseException("Duplicate group name");
        }
    }

    public void deleteGroup(String actionAuthor, long id) throws UnauthorizedException {
        LOGGER.debug("Entering deleteGroup method.");
        try{
            if(id == 1){
                throw new UnauthorizedException("Default Group cannot be deleted");
            }
            Group group = em.createNamedQuery("Group.findGroupById", Group.class).setParameter("id", id).getSingleResult();
            em.remove(group);
            AuditLog logRecord = new AuditLog("DELETE_GROUP_BY_ID", actionAuthor, gson.toJson(group.getGroupInfo()));
            em.persist(logRecord);
        } catch (Exception e) {
            throw e;
        }
    }

}
