package com.sumerge.program.controller;

import com.google.gson.Gson;
import com.sumerge.program.entity.AuditLog;
import com.sumerge.program.entity.User;
import com.sumerge.program.entity.UserPublicInfo;
import com.sumerge.program.entity.UserView;
import com.sumerge.program.exception.ResourceNotFoundException;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


@Stateless
public class AdminRepository {
    @PersistenceContext
    private EntityManager em;

    private Gson gson = new Gson();

    private static final Logger LOGGER =  Logger.getLogger(AdminRepository.class);

    public List<UserPublicInfo> getAllAdmins(String actionAuthor){
        LOGGER.debug("Entering getAllAdmins method.");
        try{
            TypedQuery query = em.createNamedQuery("User.findAllAdmins", User.class);
            List<UserPublicInfo> admins = query.getResultList();
            AuditLog logRecord = new AuditLog("VIEW_ALL_ADMINS", actionAuthor, gson.toJson(admins));
            em.persist(logRecord);
            return admins;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<UserPublicInfo> getDeletedAdmins(String actionAuthor){
        LOGGER.debug("Entering getDeletedAdmins method.");
        try{
            TypedQuery query = em.createNamedQuery("User.findDeletedAdmins", User.class);
            List<UserPublicInfo> deletedAdmins = query.getResultList();
            AuditLog logRecord = new AuditLog("VIEW_DELETED_ADMINS", actionAuthor, gson.toJson(deletedAdmins));
            em.persist(logRecord);
            return deletedAdmins;
        } catch (Exception e) {
            throw e;
        }
    }

    public UserView getAdminById(String actionAuthor, long id) throws ResourceNotFoundException {
        LOGGER.debug("Entering getAdminById method.");
        try{
            TypedQuery query = em.createNamedQuery("User.adminFindUserById", User.class).setParameter("id", id);
            User user = (User) query.getSingleResult();
            if(!user.getRole().equals("admin")) {
                throw new ResourceNotFoundException("Admin not found");
            }
            UserView userView = new UserView(user.getId(), user.getUserPublicInfo(), user.isDeleted());
            AuditLog logRecord = new AuditLog("VIEW_ADMIN_BY_ID", actionAuthor, gson.toJson(userView));
            em.persist(logRecord);
            return userView;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("Admin Not Found");
        }
    }

  public UserView getAdminByUsername(String actionAuthor, String username)
      throws ResourceNotFoundException {
        LOGGER.debug("Entering getAdminByUsername method.");
        try {
          TypedQuery query =
              em.createNamedQuery("User.adminFindUserByUsername", User.class)
                  .setParameter("username", username);
          User user = (User) query.getSingleResult();
          if (!user.getRole().equals("admin")) {
            throw new ResourceNotFoundException("Admin not found");
          }
          UserView userView = new UserView(user.getId(), user.getUserPublicInfo(), user.isDeleted());
          AuditLog logRecord =
              new AuditLog("VIEW_ADMIN_BY_USERNAME", actionAuthor, gson.toJson(userView));
          em.persist(logRecord);
          return userView;
        } catch (NoResultException e) {
          throw new ResourceNotFoundException("Admin Not Found");
        }
    }

}
