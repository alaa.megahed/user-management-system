package com.sumerge.program.controller;

import com.google.gson.Gson;
import com.sumerge.program.entity.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;


import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;
import org.apache.log4j.Logger;


import static com.arjuna.ats.jdbc.TransactionalDriver.password;
import static com.sumerge.program.util.Utilities.hashPassword;

@Stateless
public class UserRepository {


    @PersistenceContext
    private EntityManager em;

    private Gson gson = new Gson();

    private static final Logger LOGGER =  Logger.getLogger(UserRepository.class);


    public List<UserPublicInfo> getAllUsers(String actionAuthor){
        LOGGER.debug("Entering getAllUsers method.");
        try{
            TypedQuery query = em.createNamedQuery("User.findAllUsers", User.class);
            List<UserPublicInfo> users = query.getResultList();
            AuditLog logRecord = new AuditLog("VIEW_ALL_USERS", actionAuthor, gson.toJson(users));
            em.persist(logRecord);
            return users;
        } catch (Exception e) {
            throw e;
        }
    }



    public List<UserPublicInfo> getDeletedUsers(String actionAuthor){
        LOGGER.debug("Entering getDeletedUsers method.");
        try{
            TypedQuery query = em.createNamedQuery("User.findDeletedUsers", User.class);
            List<UserPublicInfo> deletedUsers = query.getResultList();
            AuditLog logRecord = new AuditLog("VIEW_DELETED_USERS", actionAuthor, gson.toJson(deletedUsers));
            em.persist(logRecord);
            return deletedUsers;
        } catch (Exception e) {
            throw e;
        }
    }


    public UserPublicInfo getUserById(String actionAuthor, long id) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserById method.");
        try{
            TypedQuery query = em.createNamedQuery("User.findUserById", User.class).setParameter("id", id);
            UserPublicInfo user = (UserPublicInfo) query.getSingleResult();
            AuditLog logRecord = new AuditLog("VIEW_USER_BY_ID", actionAuthor, gson.toJson(user));
            em.persist(logRecord);
            return user;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("User Not Found");
        }
    }

    public UserView getUserById2(String actionAuthor, long id) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserById2 method.");
        try{
            TypedQuery query = em.createNamedQuery("User.adminFindUserById", User.class).setParameter("id", id);
            User user = (User) query.getSingleResult();
            if(!user.getRole().equals("user")) {
                throw new ResourceNotFoundException("User not found");
            }
            UserView userView = new UserView(user.getId(), user.getUserPublicInfo(), user.isDeleted());
            AuditLog logRecord = new AuditLog("VIEW_USER_BY_ID", actionAuthor, gson.toJson(userView));
            em.persist(logRecord);
            return userView;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("User Not Found");
        }
    }

    public UserPublicInfo getUserByUsername(String actionAuthor, String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserByUsername method.");
        try{
            TypedQuery query = em.createNamedQuery("User.findUserByUsername", User.class).setParameter("username", username);
            UserPublicInfo user = (UserPublicInfo) query.getSingleResult();
            AuditLog logRecord = new AuditLog("VIEW_USER_BY_USERNAME", actionAuthor, gson.toJson(user));
            em.persist(logRecord);
            return user;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("User Not Found");
        }
    }

    public UserView getUserByUsername2(String actionAuthor, String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserByUsername2 method.");
        try{
            TypedQuery query = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", username);
            User user = (User) query.getSingleResult();
            if(!user.getRole().equals("user")) {
                throw new ResourceNotFoundException("User not found");
            }
            UserView userView = new UserView(user.getId(), user.getUserPublicInfo(), user.isDeleted());
            AuditLog logRecord = new AuditLog("VIEW_USER_BY_USERNAME", actionAuthor, gson.toJson(userView));
            em.persist(logRecord);
            return userView;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("User Not Found");
        }
    }


    public List<GroupInfo> getUserGroupsByUsername(String actionAuthor, String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserGroupsByUsername method.");
        try{
            TypedQuery query = em.createNamedQuery("User.getGroupsByUsername", User.class).setParameter("username", username);
            List<Group> groups =  (List<Group>) query.getResultList();
            List<GroupInfo> groupInfoList = new ArrayList<GroupInfo>();
            for(Group g : groups){
                if(g == null)
                    break;
                groupInfoList.add(g.getGroupInfo());
            }
            AuditLog logRecord = new AuditLog("VIEW_USER_GROUPS", actionAuthor, gson.toJson(groupInfoList));
            em.persist(logRecord);
            return groupInfoList;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("Data not found.");
        }
    }

    public User addUser(String actionAuthor, UserRegisteration userRegisteration) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        LOGGER.debug("Entering addUser method.");
        try{
//            String hashedPassword = hashPassword(userRegisteration.getPassword());
//            userRegisteration.setPassword(hashedPassword);
            User user = new User(userRegisteration);
            em.persist(user);
            AuditLog logRecord = new AuditLog("ADD_NEW_USERS", actionAuthor, gson.toJson(user));
            em.persist(logRecord);
            return user;
        } catch (Exception e){
            throw e;
        }
    }

    public User updateUser(String actionAuthor, UserPublicInfo userPublicInfo) throws ResourceNotFoundException {
        LOGGER.debug("Entering updateUser method.");
        try{
            User user = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", userPublicInfo.getUsername()).getSingleResult();
            if(userPublicInfo.getName() != null)
                user.getUserPublicInfo().setName(userPublicInfo.getName());
            if(userPublicInfo.getEmail() != null)
                user.getUserPublicInfo().setEmail(userPublicInfo.getEmail());
            em.merge(user);
            AuditLog logRecord = new AuditLog("UPDATE_USER_DATA", actionAuthor, gson.toJson(userPublicInfo));
            em.persist(logRecord);
            return user;
        } catch (NoResultException e) {
            throw new ResourceNotFoundException("No user found with this username.");
        }
        catch (Exception e){
            throw e;
        }
    }

    public void deleteUser(String actionAuthor, String username) throws UnauthorizedException {
        LOGGER.debug("Entering deleteUser method.");
        try{
            User user = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", username).getSingleResult();
            if(user.getId() == 1){
                throw new UnauthorizedException("Default admin cannot be deleted.");
            }
            user.setDeleted(true);
            em.merge(user);
            AuditLog logRecord = new AuditLog("DELETE_USER", actionAuthor, gson.toJson(user));
            em.persist(logRecord);
        } catch (Exception e) {
            throw e;
        }
    }

    public User restoreDeletedUser(String actionAuthor, String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering restoreDeletedUser method.");
        try{
            User user = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", username).getSingleResult();
            user.setDeleted(false);
            em.merge(user);
            UserView userView = new UserView(user.getId(), user.getUserPublicInfo(), user.isDeleted());
            AuditLog logRecord = new AuditLog("RESTORE_DELETED_USER_BY_USERNAME", actionAuthor, gson.toJson(userView));
            em.persist(logRecord);
            return user;
        } catch (NoResultException e){
            throw new ResourceNotFoundException("No user found");
        }
    }

    public void resetPassword(UserResetPassword userResetPassword) throws DatabaseException, ResourceNotFoundException, UnauthorizedException {
        LOGGER.debug("Entering resetPassword method.");
        try{
//            userResetPassword.setOldPassword(hashPassword(userResetPassword.getOldPassword()));
//            userResetPassword.setNewPassword(hashPassword(userResetPassword.getNewPassword()));
//            userResetPassword.setConfirmPassword(hashPassword(userResetPassword.getConfirmPassword()));
            User user = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", userResetPassword.getUsername()).getSingleResult();
            if(!user.getPassword().equals(userResetPassword.getOldPassword()))
                throw new UnauthorizedException("Not allowed to change password");
            if(!userResetPassword.getNewPassword().equals(userResetPassword.getConfirmPassword()))
                throw new DatabaseException("New password does not match confirm password");
            user.setPassword(userResetPassword.getNewPassword());
            em.merge(user);
            AuditLog logRecord = new AuditLog("RESTORE_DELETED_USER_BY_USERNAME", userResetPassword.getUsername(), gson.toJson(user.getUserPublicInfo()));
            em.persist(logRecord);
        } catch (NoResultException e){
            throw new ResourceNotFoundException("No user found");
        } catch (Exception e){
            throw e;
        }
    }

    public void addToGroup(String actionAuthor, long groupId, String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering addToGroup method.");
        try{
            User user = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", username).getSingleResult();
            Group group = em.createNamedQuery("Group.findGroupById", Group.class).setParameter("id", groupId).getSingleResult();
            List<Group> groups = user.getGroups();
            groups.add(group);
            user.setGroups(groups);
            em.merge(user);
        } catch (NoResultException e){
            throw new ResourceNotFoundException("User or group not found");
        }
    }

    public void removeFromGroup(String actionAuthor, long groupId, String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering removeFromGroup method.");
        try{
            User user = em.createNamedQuery("User.adminFindUserByUsername", User.class).setParameter("username", username).getSingleResult();
            Group group = em.createNamedQuery("Group.findGroupById", Group.class).setParameter("id", groupId).getSingleResult();
            List<Group> groups = user.getGroups();
            for(Group g : groups){
                if(g.getGroupInfo().getId() == groupId){
                    groups.remove(g);
                    break;
                }
            }
            user.setGroups(groups);
            em.merge(user);
        } catch (NoResultException e){
            throw new ResourceNotFoundException("User or group not found");
        }
    }

}