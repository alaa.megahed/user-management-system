package com.sumerge.program.exception;

import com.google.gson.Gson;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class DatabaseExceptionMapper implements ExceptionMapper<DatabaseException> {

    private Gson gson = new Gson();
    @Override
    public Response toResponse(DatabaseException e) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(gson.toJson(e.getMessage()))
                .build();
    }
}
