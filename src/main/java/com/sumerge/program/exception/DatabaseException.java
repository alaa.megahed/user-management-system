package com.sumerge.program.exception;

import java.sql.SQLIntegrityConstraintViolationException;

public class DatabaseException extends SQLIntegrityConstraintViolationException {
    public DatabaseException() {
        super();
    }

    public DatabaseException(String message) {
        super(message);
    }
}
