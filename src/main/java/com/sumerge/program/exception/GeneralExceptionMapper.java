package com.sumerge.program.exception;

import com.google.gson.Gson;

import javax.transaction.RollbackException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GeneralExceptionMapper implements ExceptionMapper<Exception > {
    Gson gson = new Gson();

    @Override
    public Response toResponse(Exception exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        .entity(gson.toJson(exception.getMessage()))
        .build();
        }
}
