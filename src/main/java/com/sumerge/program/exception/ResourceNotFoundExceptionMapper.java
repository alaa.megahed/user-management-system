package com.sumerge.program.exception;


import com.google.gson.Gson;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ResourceNotFoundExceptionMapper implements ExceptionMapper<ResourceNotFoundException> {

    private Gson gson = new Gson();
    @Override
    public Response toResponse(ResourceNotFoundException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(gson.toJson(exception.getMessage()))
                .build();
    }
}

