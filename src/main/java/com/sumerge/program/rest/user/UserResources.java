package com.sumerge.program.rest.user;

import com.sumerge.program.controller.UserRepository;
import com.sumerge.program.entity.*;
import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("users")
public class UserResources implements UserClient{

    private static final Logger LOGGER = Logger.getLogger(UserResources.class);

    @Context
    private SecurityContext securityContext;


    @EJB
    private UserRepository repo;


    @GET
    @Path("all")
    public Response getAllUsers() {
        LOGGER.debug("Entering getAllUsers REST method.");
        return Response.ok().entity(repo.getAllUsers(securityContext.getUserPrincipal().toString())).build();
    }


    @GET
    @Path("deleted")
    public Response getAllDeletedUsers() throws UnauthorizedException {
        LOGGER.debug("Entering getAllDeletedUsers REST method.");
        if(!securityContext.isUserInRole("admin")) {
            throw new UnauthorizedException("You are not allowed to view deleted users");
        }
        return Response.ok().entity(repo.getDeletedUsers(securityContext.getUserPrincipal().toString())).build();
    }

    @GET
    @Path("{id}")
    public Response getUserById(@PathParam("id") long id) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserById REST method.");
        if(securityContext.isUserInRole("admin")) {
            return Response.ok().entity(repo.getUserById2(securityContext.getUserPrincipal().toString(), id)).build();
        }
        return Response.ok().entity(repo.getUserById(securityContext.getUserPrincipal().toString(), id)).build();
    }

    @GET
    public Response getUserByUsername(@QueryParam("username") String username) throws ResourceNotFoundException {
        LOGGER.debug("Entering getUserByUsername REST method.");
        if(securityContext.isUserInRole("admin")) {
            return Response.ok().entity(repo.getUserByUsername2(securityContext.getUserPrincipal().toString(), username)).build();
        }
        return Response.ok().entity(repo.getUserByUsername(securityContext.getUserPrincipal().toString(), username)).build();
    }


    @GET
    @Path("{username}/groups")
    public Response getUserGroupsByUsername(@PathParam("username") String username) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering getUserGroupsByUsername REST method.");
        if(!securityContext.isUserInRole("admin") && !username.equals(securityContext.getUserPrincipal().toString()))
            throw new UnauthorizedException("You are not allowed to access this data");
        return Response.ok().entity(repo.getUserGroupsByUsername(securityContext.getUserPrincipal().toString(), username)).build();
    }


    @POST
    @Path("add")
    public Response addNewUser(UserRegisteration userRegisteration) throws UnauthorizedException, UnsupportedEncodingException, NoSuchAlgorithmException {
        LOGGER.debug("Entering addNewUser REST method.");
        if(!securityContext.isUserInRole("admin")){
            throw new UnauthorizedException("You are not allowed to add new users");
        }
        return Response.ok().entity(repo.addUser(securityContext.getUserPrincipal().toString(), userRegisteration)).build();
    }

    @PUT
    @Path("update")
    public Response updateUser(UserPublicInfo userPublicInfo) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering updateUser REST method.");
        if(!securityContext.isUserInRole("admin") && !securityContext.getUserPrincipal().toString().equals(userPublicInfo.getUsername()))
            throw new UnauthorizedException("You are not allowed to update users' data");
        return Response.ok().entity(repo.updateUser(securityContext.getUserPrincipal().toString(), userPublicInfo)).build();
    }

    @DELETE
    @Path("delete")
    public Response deleteUser(@QueryParam("username") String username) throws UnauthorizedException {
        LOGGER.debug("Entering deleteUser REST method.");
        if(!securityContext.isUserInRole("admin"))
            throw new UnauthorizedException("You are not allowed to delete users");
        repo.deleteUser(securityContext.getUserPrincipal().toString(), username);
        return Response.ok().entity("User has been deleted successfully.").build();
    }

    @PUT
    @Path("restore")
    public Response restoreDeletedUser(@QueryParam("username") String username) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering restoreDeletedUser REST method.");
        if(!securityContext.isUserInRole("admin"))
            throw new UnauthorizedException("You are not allowed to restore deleted users");

        return Response.ok().entity(repo.restoreDeletedUser(securityContext.getUserPrincipal().toString(), username)).build();

    }

    @PUT
    @Path("reset-password")
    public Response resetPassword(UserResetPassword resetPassword) throws UnauthorizedException, DatabaseException, ResourceNotFoundException {
        LOGGER.debug("Entering resetPassword REST method.");
        if(!securityContext.getUserPrincipal().toString().equals(resetPassword.getUsername()))
            throw new UnauthorizedException("You are not allowed to change other users' passwords");
        repo.resetPassword(resetPassword);
        return Response.ok().entity("Password has been successfully updated").build();
    }

    @PUT
    @Path("add-to-group")
    public Response addToGroup(@QueryParam("username") String username, @QueryParam("groupId") long groupId) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering addToGroup REST method.");
        if(!securityContext.isUserInRole("admin"))
            throw new UnauthorizedException("You are not allowed to add users to groups");
        repo.addToGroup(securityContext.getUserPrincipal().toString(), groupId, username);
        return Response.ok().entity("User is added to the group.").build();
    }

    @PUT
    @Path("remove-from-group")
    public Response removeFromGroup(@QueryParam("username") String username, @QueryParam("groupId") long groupId) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering removeFromGroup REST method.");
        if(!securityContext.isUserInRole("admin"))
            throw new UnauthorizedException("You are not allowed to delete users from groups");
        repo.removeFromGroup(securityContext.getUserPrincipal().toString(), groupId, username);
        return Response.ok().entity("{User is deleted from the group.}").build();
    }
}