package com.sumerge.program.rest.user;

import com.sumerge.program.entity.User;
import com.sumerge.program.entity.UserPublicInfo;
import com.sumerge.program.entity.UserRegisteration;
import com.sumerge.program.entity.UserResetPassword;
import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
//@Path("users")
public interface UserClient {

    @GET
    Response getAllUsers();

    @GET
    Response getAllDeletedUsers() throws UnauthorizedException;

    @GET
    Response getUserById(@PathParam("id") long id) throws ResourceNotFoundException ;

    @GET
    Response getUserByUsername(@QueryParam("username") String username) throws ResourceNotFoundException;

    @GET
    public Response getUserGroupsByUsername(@PathParam("username") String username) throws UnauthorizedException, ResourceNotFoundException;

    @POST
    Response addNewUser(UserRegisteration userRegisteration) throws UnauthorizedException, UnsupportedEncodingException, NoSuchAlgorithmException;

    @PUT
    Response updateUser(UserPublicInfo userPublicInfo) throws UnauthorizedException, ResourceNotFoundException;

    @DELETE
    Response deleteUser(@QueryParam("username") String username) throws UnauthorizedException;

    @PUT
    Response restoreDeletedUser(@QueryParam("username") String username) throws UnauthorizedException, ResourceNotFoundException;

    @PUT
    Response resetPassword(UserResetPassword resetPassword) throws UnauthorizedException, DatabaseException, ResourceNotFoundException;

    @PUT
    Response addToGroup(@QueryParam("username") String username, @QueryParam("groupId") long groupId) throws UnauthorizedException, ResourceNotFoundException;

    @PUT
    public Response removeFromGroup(@QueryParam("username") String username, @QueryParam("groupId") long groupId) throws UnauthorizedException, ResourceNotFoundException;
}