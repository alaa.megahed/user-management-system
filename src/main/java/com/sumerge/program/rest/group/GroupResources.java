package com.sumerge.program.rest.group;

import com.sumerge.program.controller.GroupRepository;
import com.sumerge.program.entity.Group;
import com.sumerge.program.entity.GroupInfo;
import com.sumerge.program.entity.UserPublicInfo;
import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("groups")
public class GroupResources implements GroupClient{

    private static final Logger LOGGER = Logger.getLogger(GroupResources.class);

    @Context
    private SecurityContext securityContext;

    @EJB
    private GroupRepository repo;

    @GET
    @Path("all")
    public Response getAllGroups() throws UnauthorizedException {
        LOGGER.debug("Entering getAllGroups REST method.");
        if(!securityContext.isUserInRole("admin")) {
            throw new UnauthorizedException("You are not allowed to access this data");
        }
        return Response.ok().entity(repo.getAllGroups(securityContext.getUserPrincipal().toString())).build();
    }

    @GET
    @Path("{id}/members")
    public Response getGroupMembers(@PathParam("id") long id) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering getGroupMembers REST method.");
        return Response.ok().entity(repo.getGroupMembers(securityContext.getUserPrincipal().toString(), securityContext.isUserInRole("admin"), id)).build();
    }

    @GET
    @Path("{id}")
    public Response getGroupById(@PathParam("id") long id) throws UnauthorizedException, ResourceNotFoundException {
        LOGGER.debug("Entering getGroupById REST method.");

        return Response.ok().entity(repo.getGroupById(securityContext.getUserPrincipal().toString(), securityContext.isUserInRole("admin"), id)).build();
    }


    @POST
    @Path("add")
    public Response addGroup(@QueryParam("name") String name) throws UnauthorizedException, DatabaseException {
        LOGGER.debug("Entering addGroup REST method.");
        if(!securityContext.isUserInRole("admin")) {
            throw new UnauthorizedException("You are not allowed to create groups");
        }
        return Response.ok().entity(repo.addGroup(securityContext.getUserPrincipal().toString(), name)).build();
    }

    @DELETE
    @Path("{id}/delete")
    public Response deleteGroup(@PathParam("id") long id) throws UnauthorizedException {
        LOGGER.debug("Entering deleteGroup REST method.");
        if(!securityContext.isUserInRole("admin")) {
            throw new UnauthorizedException("You are not allowed to delete groups");
        }
        repo.deleteGroup(securityContext.getUserPrincipal().toString(), id);
        return Response.ok().entity("Group has been deleted successfully.").build();
    }
}
