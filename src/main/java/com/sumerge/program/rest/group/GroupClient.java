package com.sumerge.program.rest.group;

import com.sumerge.program.entity.Group;
import com.sumerge.program.entity.GroupInfo;
import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("groups")
public interface GroupClient {
    @GET
    Response getAllGroups() throws UnauthorizedException;

    @GET
    Response getGroupMembers(@PathParam("id") long id) throws UnauthorizedException, ResourceNotFoundException;

    @GET
    Response getGroupById(@PathParam("id") long id) throws UnauthorizedException, ResourceNotFoundException;

    @POST
    Response addGroup(@QueryParam("name") String name) throws UnauthorizedException, DatabaseException;

    @DELETE
    Response deleteGroup(@PathParam("id") long id) throws UnauthorizedException;
}


