package com.sumerge.program.rest.admin;

import com.sumerge.program.controller.AdminRepository;
import com.sumerge.program.entity.User;
import com.sumerge.program.entity.UserPublicInfo;
import com.sumerge.program.entity.UserView;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("admins")
public class AdminResources implements AdminClient {

    private static final Logger LOGGER = Logger.getLogger(AdminResources.class);

    @Context
    private SecurityContext securityContext;

    @EJB
    private AdminRepository repo;


    @Override
    @GET
    @Path("all")
    public Response getAllAdmins() throws UnauthorizedException {
        LOGGER.debug("Entering getAllAdmins REST method.");
        if(!securityContext.isUserInRole("admin")) {
            throw new UnauthorizedException("You are not allowed to view system admins");
        }
        return Response.ok().entity(repo.getAllAdmins(securityContext.getUserPrincipal().toString())).build();
    }

    @Override
    @GET
    @Path("deleted")
    public Response getAllDeletedAdmins() throws UnauthorizedException {
        LOGGER.debug("Entering getAllDeletedAdmins REST method.");
        if(!securityContext.isUserInRole("admin")) {
            throw new UnauthorizedException("You are not allowed to view deleted admins");
        }
        return Response.ok().entity(repo.getDeletedAdmins(securityContext.getUserPrincipal().toString())).build();
    }

    @GET
    @Path("{id}")
    public Response getAdminById(@PathParam("id") long id) throws ResourceNotFoundException, UnauthorizedException {
        LOGGER.debug("Entering getAdminById REST method.");
        if(securityContext.isUserInRole("admin")) {
            return Response.ok().entity(repo.getAdminById(securityContext.getUserPrincipal().toString(), id)).build();
        }
        throw new UnauthorizedException("You are not allowed to access this data");
    }

    @GET
    public Response getAdminByUsername(@QueryParam("username") String username) throws ResourceNotFoundException, UnauthorizedException {
        LOGGER.debug("Entering getAdminByUsername REST method.");
        if(securityContext.isUserInRole("admin")) {
            return Response.ok().entity(repo.getAdminByUsername(securityContext.getUserPrincipal().toString(), username)).build();
        }
        throw new UnauthorizedException("You are not allowed to access this data");
    }
}
