package com.sumerge.program.rest.admin;

import com.sumerge.program.entity.User;
import com.sumerge.program.entity.UserPublicInfo;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import java.util.List;

@Path("admins")
public interface AdminClient {

    @GET
    Response getAllAdmins() throws UnauthorizedException;

    @GET
    Response getAllDeletedAdmins() throws UnauthorizedException;

    @GET
    public Response getAdminById(@PathParam("id") long id) throws ResourceNotFoundException, UnauthorizedException;

    @GET
    public Response getAdminByUsername(@QueryParam("username") String username) throws ResourceNotFoundException, UnauthorizedException;

}
