package com.sumerge.program.integrationTest;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.sumerge.program.entity.*;
import com.sumerge.program.exception.DatabaseException;
import com.sumerge.program.exception.ResourceNotFoundException;
import com.sumerge.program.exception.UnauthorizedException;
import com.sumerge.program.rest.user.UserClient;
import com.sumerge.program.util.Utilities;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import static org.junit.Assert.*;

public class UserIT {

    private static final int USER_COUNT = 1;
    private static final int DELETED_USER_COUNT = 2;

    private static UserClient client;

    private static List<Object> providers;


    @BeforeClass
    public static void init() {
        providers = new ArrayList<Object>();
        providers.add(new JacksonJaxbJsonProvider());
        client = JAXRSClientFactory.create("http://127.0.0.1:8880",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
    }


    @Test
    public void authorizationTest() {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/all",
                UserClient.class, providers, "default-admin", "wrongPassword", null);
        Response response = client.getAllUsers();
        assertEquals(401, response.getStatus());
    }

    @Test
    public void getAllUsersTest1()  {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/all",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = client.getAllUsers();
        assertNotNull(response);
        List<UserPublicInfo> users = response.readEntity(List.class);

        assertEquals(200, response.getStatus());
        assertEquals(USER_COUNT, users.size());
    }


    @Test
    public void getAllUsersTest2(){
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/all",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = client.getAllUsers();
        assertNotNull(response);
        List<UserPublicInfo> users = response.readEntity(List.class);

        assertEquals(200, response.getStatus());
        assertEquals(USER_COUNT, users.size());
    }


    @Test(expected = ForbiddenException.class)
    public void getDeletedUsersTest1() {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/deleted",
                    UserClient.class, providers, "user1", "user1NewPass", null);

        try {
            Response response = client.getAllDeletedUsers();
        } catch (UnauthorizedException e) {
            e.printStackTrace();
        }
      }

      @Test
      public void getDeletedUsersTest2(){
          client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/deleted",
                  UserClient.class, providers, "default-admin", "defaultAdminPass", null);
          Response response = null;
          try {
              response = client.getAllDeletedUsers();
          } catch (UnauthorizedException e) {
              e.printStackTrace();
          }
          assertNotNull(response);
          List<UserPublicInfo> deletedUsers = response.readEntity(List.class);
          assertEquals(DELETED_USER_COUNT, deletedUsers.size());
    }

    @Test
    public void getUserByUsernameTest1() throws IOException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = null;

        response = client.getUserByUsername("user1");

        assertNotNull(response);
        InputStream io = (InputStream) response.getEntity();
        String body = IOUtils.toString(io, "UTF-8");
        System.out.println(body);
        assertEquals(200, response.getStatus());
    }

    @Test(expected = BadRequestException.class)
    public void getUserByUsernameTest2() throws ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = null;
        response = client.getUserByUsername("nonExistingUser");

        assertEquals(400, response.getStatus());
    }

    @Test
    public void getUserByUsernameTest3() throws IOException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = null;

        response = client.getUserByUsername("user1");

        assertNotNull(response);
        InputStream io = (InputStream) response.getEntity();
        String body = IOUtils.toString(io, "UTF-8");
        System.out.println(body);
        assertEquals(200, response.getStatus());
    }

    @Test
    public void getUserByIdTest1() throws IOException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/2",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = null;

        response = client.getUserById(2);

        assertNotNull(response);
        InputStream io = (InputStream) response.getEntity();
        String body = IOUtils.toString(io, "UTF-8");
        System.out.println(body);
        assertEquals(200, response.getStatus());
    }

    @Test(expected = BadRequestException.class)
    public void getUserByIdTest2() throws IOException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/1",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = null;

        response = client.getUserById(1);

        assertEquals(400, response.getStatus());
        assertNotNull(response);
    }

    @Test(expected = ForbiddenException.class)
    public void addUserTest1() throws UnsupportedEncodingException, NoSuchAlgorithmException, UnauthorizedException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/add",
                UserClient.class, providers, "user1", "user1NewPass", null);
        UserRegisteration userRegisteration = new UserRegisteration("new-user", Utilities.hashPassword("newUserPass"), "New User", "new.user@email", "user");
        Response response = null;

        response = client.addNewUser(userRegisteration);

        assertEquals(401, response.getStatus());
    }

    @Test
    public void addUserTest2() throws UnsupportedEncodingException, NoSuchAlgorithmException, UnauthorizedException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/add",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        UserRegisteration userRegisteration = new UserRegisteration("new-user4", Utilities.hashPassword("newUser4Pass"), "New User2", "new.user2@email", "user");
        Response response = null;

        response = client.addNewUser(userRegisteration);

        assertEquals(200, response.getStatus());
        User user = response.readEntity(User.class);
        assertNotNull(user);
    }

    @Test(expected = InternalServerErrorException.class)
    public void addUserTest3() throws UnsupportedEncodingException, NoSuchAlgorithmException, UnauthorizedException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/add",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        UserRegisteration userRegisteration = new UserRegisteration("new-user2", Utilities.hashPassword("newUser2Pass"), "New User2", "new.user2@email", "user");
        Response response = null;

        response = client.addNewUser(userRegisteration);

        assertEquals(500, response.getStatus());
    }

    @Test(expected = ForbiddenException.class)
    public void updateUserTest1() throws UnsupportedEncodingException, NoSuchAlgorithmException, UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/update",
                UserClient.class, providers, "user1", "user1NewPass", null);
        UserPublicInfo userPublicInfo = new UserPublicInfo("new-user", "Updated Name", null);
        Response response = null;

        response = client.updateUser(userPublicInfo);

        assertEquals(401, response.getStatus());
    }

    @Test
    public void updateUserTest2() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/update",
                UserClient.class, providers, "new-user4", "newUser4Pass", null);
        UserPublicInfo userPublicInfo = new UserPublicInfo("new-user4", "Updated Name", null);
        Response response = null;
        response = client.updateUser(userPublicInfo);

        assertEquals(200, response.getStatus());
        User user = response.readEntity(User.class);
        assertEquals("Updated Name", user.getUserPublicInfo().getName());
    }

    @Test
    public void updateUserTest3() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/update",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        UserPublicInfo userPublicInfo = new UserPublicInfo("new-user4", "Updated Name2", null);
        Response response = null;

        response = client.updateUser(userPublicInfo);

        assertEquals(200, response.getStatus());
        User user = response.readEntity(User.class);
        assertEquals("Updated Name2", user.getUserPublicInfo().getName());
    }

    @Test(expected = ForbiddenException.class)
    public void addUserToGroupTest1() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/add-to-group",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = null;
        response = client.addToGroup("new-user4",1);
        assertEquals(401, response.getStatus());
    }

    @Test
    public void addUserToGroupTest2() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/add-to-group",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = null;

        response = client.addToGroup("new-user4",1);


        assertEquals(200, response.getStatus());
    }

    @Test
    public void getUserGroupsTest1(){
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/new-user4/groups",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = null;
        try {
            response = client.getUserGroupsByUsername("new-user4");
        } catch (UnauthorizedException | ResourceNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(200, response.getStatus());
        List groups = response.readEntity(List.class);
        assertEquals(1, groups.size());
        assertEquals(1, ((LinkedHashMap)groups.get(0)).get("id"));
    }

    @Test
    public void getUserGroupsTest2(){
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/new-user4/groups",
                UserClient.class, providers, "new-user4", "newUser4Pass", null);
        Response response = null;
        try {
            response = client.getUserGroupsByUsername("new-user4");
        } catch (UnauthorizedException | ResourceNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(200, response.getStatus());
        List groups = response.readEntity(List.class);
        assertEquals(1, groups.size());
        assertEquals(1, ((LinkedHashMap)groups.get(0)).get("id"));
    }

    @Test(expected = ForbiddenException.class)
    public void getUserGroupsTest3() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/new-user4/groups",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = null;

        response = client.getUserGroupsByUsername("new-user4");

        assertEquals(401, response.getStatus());
    }

    @Test(expected = ForbiddenException.class)
    public void removeUserFromGroupTest1() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/remove-from-group",
                UserClient.class, providers, "new-user4", "newUser4Pass", null);
        Response response = null;

        response = client.removeFromGroup("new-user4", 1);

        assertEquals(401, response.getStatus());
    }

    @Test
    public void removeUserFromGroupTest2() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/remove-from-group",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = null;

        response = client.removeFromGroup("new-user4", 1);

        assertEquals(200, response.getStatus());
        assertEquals("{User is deleted from the group.}", response.readEntity(String.class));
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/new-user4/groups",
                UserClient.class, providers, "new-user4", "newUser4Pass", null);
        Response response2 = null;

        response2 = client.getUserGroupsByUsername("new-user4");

        assertEquals(200, response2.getStatus());
        List groups = response2.readEntity(List.class);
        assertEquals(0, groups.size());
    }

    @Test(expected = ForbiddenException.class)
    public void resetPasswordTest1() throws UnsupportedEncodingException, NoSuchAlgorithmException, ResourceNotFoundException, DatabaseException, UnauthorizedException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/reset-password",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = null;
        UserResetPassword userResetPassword = new UserResetPassword("new-user4",Utilities.hashPassword("newUser4Pass"), Utilities.hashPassword("user4Pass"), Utilities.hashPassword("user4Pass"));

        response = client.resetPassword(userResetPassword);


        assertEquals(401, response.getStatus());
    }

    @Test
    public void resetPasswordTest2() throws UnsupportedEncodingException, NoSuchAlgorithmException, ResourceNotFoundException, DatabaseException, UnauthorizedException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/reset-password",
                UserClient.class, providers, "user1", "user1NewPass", null);
        Response response = null;
        UserResetPassword userResetPassword = new UserResetPassword("user1",Utilities.hashPassword("user1NewPass"), Utilities.hashPassword("user1Pass"), Utilities.hashPassword("user1Pass"));

        response = client.resetPassword(userResetPassword);


        assertEquals(200, response.getStatus());
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/all",
                UserClient.class, providers, "user1", "user1Pass", null);
        Response response2 = client.getAllUsers();
        assertEquals(200, response2.getStatus());
    }

    @Test(expected = ForbiddenException.class)
    public void deleteUserTest1() throws UnauthorizedException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/delete",
                UserClient.class, providers, "user1", "user1Pass", null);
        Response response = client.deleteUser("new-user4");
        assertEquals(401, response.getStatus());
    }

    @Test(expected = BadRequestException.class)
    public void deleteUserTest2() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/delete",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = client.deleteUser("new-user4");
        assertEquals(200, response.getStatus());
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/",
                UserClient.class, providers, "user1", "user1Pass", null);
        Response response2 = null;

        response2 = client.getUserByUsername("new-user4");

        assertEquals(400, response2.getStatus());
    }

    @Test
    public void restoreDeletedUserTest1() throws UnauthorizedException, ResourceNotFoundException {
        client = JAXRSClientFactory.create("http://127.0.0.1:8880/users/restore",
                UserClient.class, providers, "default-admin", "defaultAdminPass", null);
        Response response = client.restoreDeletedUser("new-user4");
        assertEquals(200, response.getStatus());
        User user = response.readEntity(User.class);
        assertEquals("new-user4", user.getUserPublicInfo().getUsername());
    }
}
